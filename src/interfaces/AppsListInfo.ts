import AppInfo from '@/interfaces/AppInfo';

export default interface AppsListInfo {
  apps: Array<AppInfo>;
  pageCount: number;
}