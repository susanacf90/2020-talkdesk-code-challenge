import Vue from 'vue';
import Vuex from 'vuex';
import AppInfo from '@/interfaces/AppInfo';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    search: '',
    apps: new Array<AppInfo>(),
    selectedCategory: '',
  },
  mutations: {
    updateSearch (state, searchText) {
      state.search = searchText;
    },
    updateApps (state, apps) {
      state.apps = apps;
    },
    updateSelectedCategory (state, category) {
      state.selectedCategory = category;
    }
  }
});