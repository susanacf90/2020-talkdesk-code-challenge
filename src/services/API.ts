import apps from "@/../apps.json";
import AppInfo from '@/interfaces/AppInfo';
import AppsListInfo from '@/interfaces/AppsListInfo';

export default {
  // Returns the pageSize number of apps based on the selected category, the page the user is in and the search terms.
  // It also returns the total page count.
  getApps: function (category: string, searchWord: string, pageNumber: number, pageSize: number): Promise<AppsListInfo> {
    const filteredApps = apps.slice().filter(function (app: AppInfo) {
      if (!category) {
        return app.name.toLowerCase().indexOf(searchWord.toLowerCase()) >= 0;
      }

      return app.categories.includes(category) && app.name.toLowerCase().indexOf(searchWord.toLowerCase()) >= 0;
    }).sort(this.byPrice);

    return new Promise((resolve) => {
      setTimeout(() => (resolve({ apps: filteredApps.slice(pageSize * pageNumber - pageSize, pageSize * pageNumber), pageCount: Math.ceil(filteredApps.length / pageSize) })), 2000);
    });
  },

  // Gets all the existing app categories.
  getCategories: function (): Promise<Array<string>> {
    const categories: Set<string> =  new Set(apps.map(app => app.categories).flat());
    
    return new Promise((resolve) => {
      setTimeout(() => (resolve(Array.from(categories).sort(this.byName))), 500);
    });
  },

  // Function to order items by price.
  byPrice: function (a: AppInfo, b: AppInfo) {
    const aSum = a.subscriptions.reduce((acc, cur) => (acc + cur.price), 0);
    const bSum = b.subscriptions.reduce((acc, cur) => (acc + cur.price), 0);


    return aSum - bSum;
  },

  // Function to order items by name.
  byName: function (a: string, b: string) {
    if (a < b)
      return -1;
    else if (b < a)
      return 1;

    return 0;
  }
}