import Subscription from '@/interfaces/Subscription';

export default interface AppInfo {
  id: string;
  name: string;
  description: string;
  categories: Array<string>;
  subscriptions: Array<Subscription>;
}