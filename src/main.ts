import Vue from 'vue'
import App from '@/App.vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home.vue'
import store from '@/store'
import vueDebounce from 'vue-debounce';

Vue.config.productionTip = false

Vue.use(VueRouter);
Vue.use(vueDebounce);

const routes = [
  { path: '/', component: Home },
  { path: '/index.html', redirect: '/' },
  { path: '/home', redirect: '/' },
];

const router = new VueRouter({
  mode: 'history',
  routes,
  scrollBehavior() {
    return { x: 0, y: 0 };
  }
});

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
